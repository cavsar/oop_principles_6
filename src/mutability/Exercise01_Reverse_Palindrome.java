package mutability;

public class Exercise01_Reverse_Palindrome {
    /*
    Write a method that takes a String and returns it back reversed

    Hello -> olleH
    Java -> avaJ
      ->
    a  -> a
    1234 -> 4321
    Good morning -> gninrom dooG
      */

    public static String reverseString(String str) {

        return new StringBuilder(str).reverse().toString();
    }

    public static boolean isPalindrome(String str) {
        StringBuilder sb = new StringBuilder(str);
        String reversed = sb.reverse().toString();
        return str.equals(reversed);
    }

    public static void main(String[] args) {

        System.out.println(reverseString("Hello"));
        System.out.println(reverseString("Java"));
        System.out.println(reverseString("Good morning"));
        System.out.println(isPalindrome("civic"));
        System.out.println(isPalindrome("word"));
    }
}
