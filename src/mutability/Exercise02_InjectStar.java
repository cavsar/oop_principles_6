package mutability;

public class Exercise02_InjectStar {
    public static String injectStar(String str){
        if(str.length() <= 1) return "";
        else if(str.length() % 2 == 0) return  str.substring(0, str.length()/2) + "*" + str.substring(str.length()/2);
        return str.substring(0, str.length()/2) + "*" + str.charAt(str.length()/2) + "*" + str.substring(str.length()/2+1);
    /* 2nd way

     */

    }


    public static void main(String[] args) {
        System.out.println(injectStar("aaaa"));
        System.out.println(injectStar("aaaaa"));

    }
}
