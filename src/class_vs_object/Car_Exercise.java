package class_vs_object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Car_Exercise {
    public static void main(String[] args) {
        Car car1 = new Car("Honda", "Civic", 2015, 10000);
        Car car2 = new Car("BMW", "X5", 2023, 60000);
        Car car3 = new Car("Tesla", "S", 2023, 100000);
        Car car4 = new Car("Toyota", "Camry", 2008, 8000);

        System.out.println("\n=======before removing========\n");
        ArrayList<Car> cars = new ArrayList<>(Arrays.asList(car1, car2, car3, car4));
        System.out.println(cars);
        System.out.println(cars.size());

        System.out.println("\n-----------After removing---------\n");
        //cars.removeIf(car -> car.price > 30000 || car.year < 2010);

        Iterator<Car> iterator = cars.iterator();

        while (((Iterator<?>) iterator).hasNext()) {
            Car current = iterator.next();
            if (current.year < 2010 || current.price > 30000) iterator.remove();
        }

        System.out.println(cars); // Only Honda
        System.out.println(cars.size()); // 1
    }}