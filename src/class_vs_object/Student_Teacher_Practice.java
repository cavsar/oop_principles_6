package class_vs_object;

import java.util.ArrayList;

public class Student_Teacher_Practice {
    public static void main(String[] args) {
        /*
        Create 3 students object with below info
        s1
        s2
        s3
         */
        Student s1 = new Student("John Doe", 45);
        Student s2 = new Student("Jane Doe", 35);
        Student s3 = new Student("Alex Smith", 60);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);

        Teacher t1 = new Teacher("Adam Nasr");
        Teacher t2 = new Teacher("Alina Graur");
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(t1);
        System.out.println(t2);
        s1.teacher=t1;
        s2.teacher=t2;
        s3.teacher=t1;
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(t1);
        System.out.println(t2);


    }
}
