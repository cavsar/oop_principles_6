package access_modifiers.package2;

import access_modifiers.Table;
import access_modifiers.package1.Honda;
import javafx.scene.control.Tab;

public class Test {
    public static void main(String[] args) {
     //   BMW b1 = new BMW();// is not accessible from another package
        Honda h1 = new Honda();

        Table.DinnerTable.dinnerTableMethod();
    }
}
