package access_modifiers.package1;

class BMW {//Accessible only in the same package and same class

    public static void main(String[] args) {
        BMW b1 = new BMW();
    }
}
